// src/components/About.js
import React from 'react';
import '../About.css'; // Import the CSS file for About

const About = () => {
  return (
    <div className="about-container">
      <div className="about-text">
        <h1>Welcome to IKELOS GROUP - Your Comprehensive Security Partner</h1>
        
        <p>At IKELOS GROUP, we redefine the essence of holistic security solutions. We are not just a company; we are your steadfast ally in navigating the complex landscape of security challenges in the modern world. Our suite of specialized services is meticulously designed to cater to a diverse range of security needs, ensuring your peace of mind in an ever-evolving threat landscape.</p>
        
        <h2>Why Choose IKELOS GROUP?</h2>

        <p>Expertise & Experience: Our team comprises seasoned professionals with extensive experience in various security domains.</p>

        <p>Customized Solutions: We understand that each client's needs are unique. Our solutions are tailored to meet your specific security requirements.</p>

        <p>Proactive Approach: We believe in staying ahead of threats. Our proactive strategies are designed to anticipate and neutralize threats before they materialize.</p>

        <p>Confidentiality Assured: Your security and privacy are our top priorities. We maintain the highest standards of confidentiality and discretion in all our engagements.</p>

        <p>Embark on your journey towards a secure future with IKELOS GROUP – where your security is our mission. Let us be your trusted partner in navigating the complex and dynamic world of security.</p>
      </div>
    </div>
  );
};

export default About;
